const getPilihanComputer = () => {
  Math.floor(Math.random() * 3 + 1);
  if ((computer = 1)) return "batu";
  if ((computer = 2)) return "gunting";
  if ((computer = 3)) return "kertas";
};

function getHasil(computer, player) {
  if (player == computer) return "SERI";
  if ((player === "kertas" && computer === "batu") || (player === "batu" && computer === "gunting") || (player === "gunting" && computer === "kertas")) {
    return "PLAYER 1 WIN";
  }
  if ((player === "kertas" && computer === "gunting") || (player === "batu" && computer === "kertas") || (player === "gunting" && computer === "batu")) {
    return "COM WIN";
  }
}

function putar() {
  const gambarcomp = document.querySelector(".comp li img");
  const gambar = [" batu", "gunting", "kertas"];

  //   let i = 0;
  //   const waktuMulai = new Date().getTime();
  //   setInterval(function () {
  //     if (new Date().getTime() - waktuMulai > 1000) {
  //       clearInterval;
  //       return;
  //     }
  //     gambarcomp.setAttribute("src", "assets/" + gambar[i++] + ".png");
  //     if (i == gambar.length) i = 0;
  //   }, 100);
}

const pilihan = document.querySelectorAll(".player li img");
let win = 1;
let lose = 1;
pilihan.forEach(function (pil) {
  pil.addEventListener("click", function () {
    const pilihanComputer = getPilihanComputer();
    const pilihanPlayer = pil.className;
    const hasil = getHasil(pilihanComputer, pilihanPlayer);

    putar();
    setTimeout(function () {
      //   const gambarComputer = document.querySelector(".gambarComputer");
      //   gambarComputer.setAttribute("src", "assets/" + pilihanComputer + ".png");
      const info = document.querySelector(".info");
      info.innerHTML = hasil;
      const skorComp = document.querySelector(".skorComp");
      const skorPlayer = document.querySelector(".skorPlayer");
      if (hasil == "Menang!") {
        skorPlayer.innerHTML = win++;
      }
      if (hasil == "Kalah") {
        skorComp.innerHTML = lose++;
      }
    }, 1000);
  });
});
